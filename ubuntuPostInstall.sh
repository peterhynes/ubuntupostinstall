#Post Ubuntu OS install script - Peter Hynes - March 2019
#Includes VSCode install
#Includes Curl install
#includes Vim install
#includes docker install

#Update repos
sudo apt-get update

#Vim install
sudo apt install vim

#Curl install
sudo apt install curl

#Install docker
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"

sudo apt-get update

sudo apt-get install docker-ce docker-ce-cli containerd.io

#VSCode install
#curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.gpg
#sudo install -o root -g root -m 644 microsoft.gpg /etc/apt/trusted.gpg.d/
#sudo sh -c 'echo "deb [arch=amd64] http://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list'
#sudo apt-get install apt-transport-https
#sudo apt-get update --allow-unauthenticated
#sudo apt-get install code

wget https://go.microsoft.com/fwlink/?LinkID=760868
mv index.html\?LinkID\=760868 code.deb
sudo apt install ./code.deb
"ubuntuPostInstall.sh" 45L, 1238C